# platform-layer

This repository supports deployment to 2 different environments: staging and production

## repository structure
```
├── README.md
├── deploy-platform
│   ├── helmfile.yaml
│   ├── partial
│   │   ├── environments.yaml
│   │   ├── helm.yaml
│   │   ├── infra.yaml
│   │   └── repos.yaml
│   └── values
│       ├── default
│       ├── production
│       └── staging
└── rbac
    └── staging
        └── gitlab.yaml
```

Repository contains RBAC for gitlab service account for `staging` and `production` environments.

For CI/CD used fluxcd and helm-operator.

As monitoring we have chose Prometheus operator solution with a lot of monitoring services inside including Grafana.

Loki has been picked as a logging service because it's light-weight and built-in Grafana. Google Cloud Storage is used to keep object storage data and Cassandra is used as a storage for indexes (single node in staging, and full HA cluster in production).

To be able to expose our services we've deployed ingress-nginx controller instead of provided by Google, because it's more flexible and we can have full control over it. In addition to ingress-controller we've added a cert-manager to provision certificates automatically.

TO-DO:

- [ ] external-dns

- [ ] harbor as artifactory storage with automatic upload into GCR in each google project

- [ ] Vault
